```plantuml
@startuml

actor User as User
participant Controller as "ASP.NET Core Controller"
participant EFCore as "Entity Framework Core"
participant Database as "Database"

User -> Controller: 发送请求
Controller -> EFCore: 发送数据访问请求
EFCore -> Database: 执行查询
Database --> EFCore: 返回数据
EFCore --> Controller: 返回数据
Controller --> User: 返回响应

User -> Controller: 发送数据修改请求
Controller -> EFCore: 保存更改
EFCore -> Database: 更新数据库
Database --> EFCore: 返回确认
EFCore --> Controller: 返回确认
Controller --> User: 返回响应

@enduml
```
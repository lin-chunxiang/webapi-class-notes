1. 入口程序（Program.cs）
```csharp
using Microsoft.AspNetCore.Hosting;  
using Microsoft.Extensions.Hosting;  
  
namespace MyAspNetCoreApp  
{  
    public class Program  
    {  
        public static void Main(string[] args)  
        {  
            CreateHostBuilder(args).Build().Run();  
        }  
  
        public static IHostBuilder CreateHostBuilder(string[] args) =>  
            Host.CreateDefaultBuilder(args)  
                .ConfigureWebHostDefaults(webBuilder =>  
                {  
                    webBuilder.UseStartup<Startup>();  
                });  
    }  
}
```
2. 启动类（Startup.cs）
```csharp
using Microsoft.AspNetCore.Builder;  
using Microsoft.Extensions.DependencyInjection;  
  
namespace MyAspNetCoreApp  
{  
    public class Startup  
    {  
        public void ConfigureServices(IServiceCollection services)  
        {  
            // 添加服务到容器中，比如MVC服务  
            services.AddControllers();  
        }  
  
        public void Configure(IApplicationBuilder app)  
        {  
            // 配置中间件，比如路由中间件  
            app.UseRouting();  
  
            // 添加终结点路由  
            app.UseEndpoints(endpoints =>  
            {  
                endpoints.MapControllers();  
            });  
        }  
    }  
}
```
3. 控制器（BlogsController.cs）
```csharp
using Microsoft.AspNetCore.Mvc;  
  
namespace MyAspNetCoreApp.Controllers  
{  
    [ApiController]  
    [Route("[controller]")]  
    public class BlogsController : ControllerBase  
    {  
        // GET api/blogs  
        [HttpGet]  
        public IActionResult Index()  
        {  
            return Ok("999");  
        }  
  
        // GET api/blogs/{id}  
        [HttpGet("{id}")]  
        public IActionResult Single(int id)  
        {  
            return Ok(id);  
        }  
    }  
}
```